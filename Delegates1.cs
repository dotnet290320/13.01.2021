using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Delegates1
{
    class Program
    {
        public string name1;
        public delegate void Signature_For_Function_Void_With_No_Parameters();
        public delegate float Signature_For_Function_float_with_float_parameter(float f);

        static void Invokder10Times(Signature_For_Function_Void_With_No_Parameters ms)
        {
            for (int i = 0; i < 10; i++)
            {
                ms();
            }
        }
        static void Study2(String name)
        {
            Console.WriteLine(name);
        }

        static void MyFunc_Void_No_Parameters()
        {
            Console.WriteLine("I am MyFunc_Void_No_Parameters");
        }

        static void Main(string[] args)
        {
            // functions first class member
            // delegate ==> method signature 
            //MyFunc_Void_No_Parameters();

            Thread t = new Thread(MyFunc_Void_No_Parameters);
            // 1 create a functions in delegate of ParameterizedThreadStart
            // 2 create the thread , in ctor pass the function you just created name 
            Thread t1 = new Thread() // public Thread(ParameterizedThreadStart start);
            // 3 create a delegate for function that returns int and get no parameter
            // 4 create a delefate for function that returns double and gets two dobules
            // 4.1 create a real function that gets 2 doubles and return double (which is sum of both parameters)
            // 5 create a function that gets as parameter the function type (delegate) you created in 4
            //   in this function invoke the method you created in 4.1. and print the result

            Study1(MyFunc_Void_No_Parameters);

            Study2("i love delegate!");

        }
    }
}
