            create a signature(delegate) D1 that retruns void and accepts 2 string parameters
             create a function F1 that retruns void and accepts 2 string parameters -- in this function
              concat the two strings and print in upper case
             create a function F2 that gets a method with type D1 and invokes it with "hello ", "world"
             from main call F2 and send it F1 as parameter
             from main call F2 and send it lambda expression which does the same as F1 but with lower case
             from main call F2 and send it lambda expression which print the two strings in reverse
             create a signature(delegate) D2 that retruns float and accepts 2 floats parameters
             create a function F3 that retruns float and accepts 2 floats parameters -- in this function
              RETURN the sum of both numbers
             create a function F4 that gets a method with type D2 and two floats and invokes the function with the 2 floats
             from main Console.Writeline the result of F4 and send it F3 as parameter, -4.555f, 19.4545
             from main Console.Writeline the result of F4 and send it lambda expression which perform minus 20.38 5.25
             from main Console.Writeline the result of F4 and send it lambda expression which perform multiply 14.4 60.27
             from main Console.Writeline the result of F4 and send it lambda expression which perform div,
                   but first check if not divide by zero 54.24 75.06(+also: 54.24, 0)
             from main Console.Writeline the result of F4 and send it lambda expression which perform pow 43 91.26
             from main Console.Writeline the result of F4 and send it lambda expression which returns the bigger 45.71 31.19
             from main Console.Writeline the result of F4 and send it lambda expression which returns the smaller 54.24 75.06
